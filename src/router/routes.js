import task1 from '../components/task1.vue';
import task2 from '../components/task2-parent.vue';
import task3 from '../components/task3-parent.vue';
import task4 from '../components/task4-parent.vue';
import task5 from '../components/task5-currency.vue';

const routes = [
    { path: '/task1', component: task1, name: 'task1'},
    { path: '/task2', component: task2, name: 'task2'},
    { path: '/task3', component: task3, name: 'task3'},
    { path: '/task4', component: task4, name: 'task4'},
    { path: '/task5', component: task5, name: 'task5'}
];

export default routes;