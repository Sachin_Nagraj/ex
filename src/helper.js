class user {
    constructor(userObject){
        this.name = userObject.name;
        this.age = userObject.age;
        this.city = userObject.city;
    }

    displayName(){
        console.log(this.name);
    }

    displayAge(){
        console.log(this.age);
    }

    displayCity(){
        console.log(this.city);
    }
}

export { user } 